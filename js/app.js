$(function () {

	let exercises = {};
	let lastpart = window.location.pathname.split('/').reverse()[0];
	let searchParams = new URLSearchParams(window.location.search);
	console.log(lastpart);

	if (lastpart == "") {
		$("#welcome").show();
	} else if (lastpart == "plans") {
		$("#plans-div").show();
		$("#welcome").hide();

		if (searchParams.has('id')) {
			showPlan(searchParams.get('id'));
		} else if (searchParams.has('new_id')) {
			editPlan(searchParams.get('new_id'));
		} else {
			showAllPlans();
		}
	} else if (lastpart == "users") {
		$("#users-div").show();
		$("#welcome").hide();
		showAllUsers();
	} else if (lastpart == "exercises") {
		$("#exercises-div").show();
		$("#welcome").hide();
		showAllExercises();
	} else if (lastpart == "workouts") {
		$("#workouts-div").show();
		$("#welcome").hide();
		showAllWorkouts();
	}

	$('#new-user-button').on('click', function (e) {
		console.log("yep");
		e.preventDefault();

		let formData = {
			'first_name': $('input[name=user-firstname]').val(),
			'last_name': $('input[name=user-lastname]').val(),
			'email': $('input[name=user-email]').val()
		};

		console.log(formData);
		if (formData.first_name == '' || formData.last_name == '' || formData.email == '') {
			return;
		}

		$.ajax({
			type: 'post',
			url: 'http://localhost:9000/users',
			data: formData,
			success: function () {
				showAllUsers();
			}
		});
	});

	function showAllPlans() {
		$("#plans-table").show();
		console.log('show plans table now');

		$.ajax({
			type: "GET",
			dataType: "json",
			url: `http://localhost:9000/plans`,
			success: function (response) {
				console.log(response);
				let trHTML = '<tr><th align="left" width="50">#</th><th align="left" width="100">Name</th><th align="left" width="100">Description</th><th align="left" width="100"></th><th width="10"></th><th width="10"></th></tr>';
				let data = response.data;

				let i = 1;
				$.each(data, function (key, value) {
					trHTML +=
						'<tr id="' + value.id + '" value="' + value.id + '"><td>' + i +
						'</td><td>' + value.plan_name +
						'</td><td>' + value.plan_description +
						'</td><td><a class="btn btn-primary" id="view-plan" href="/plans?id=' + value.id + '">Show</a>' +
						'</td><td><a class="btn btn-danger" id="plan-delete-button" href="/plans">Delete</a>' +
						'</td></tr>';
					i++;
				});

				$('#plans-table').html(trHTML);
			}
		});
	}

	function showAllWorkouts() {
		$("#workouts-table").show();
		console.log('show workouts table now');

		$.ajax({
			type: "GET",
			dataType: "json",
			url: `http://localhost:9000/workouts`,
			success: function (response) {
				console.log(response);
				let trHTML = ' \
					<tr> \
						<th align="left" width="50">#</th> \
						<th align="left" width="100">User Name</th>\
						<th align="left" width="100">Plan Name</th>\
						<th align="left" width="100">Plan Description</th>\
						<th align="left" width="10"></th>\
						<th align="left" width="10"></th>\
					</tr>';

				let data = response.data;
				let i = 1;

				$.each(data, function (key, value) {
					trHTML +=
						'<tr id="' + value.workout_id + '"><td>' + i +
						'</td><td>' + value.first_name + ' ' + value.last_name +
						'</td><td>' + value.plan_name +
						'</td><td>' + value.plan_description +
						'</td><td><a class="btn btn-primary" id="view-plan" href="/plans?id=' + value.plan_id + '">Show</a>' +
						'</td><td><a class="btn btn-danger" id="workout-delete-button" href="">Delete</a>' +
						'</td></tr>';
					i++;
				});

				$('#workouts-table').html(trHTML);
			}
		});
	}

	$('#workouts-table').on('click', '#workout-delete-button', function () {
		let id = $(this).closest('tr').attr('id');

		$.ajax({
			type: "DELETE",
			dataType: "json",
			url: 'http://localhost:9000/workouts?workout_id=' + id,
			success: function (response) {
				console.log('done, will now click');
				showAllWorkouts();
			}
		});
	});

	function getUsers() {
		$.ajax({
			type: "GET",
			dataType: "json",
			url: 'http://localhost:9000/exercises',
			success: function (response) {
				getUsersData(response.data);

			}
		});
	}

	function getUsersData(response) {
		exercises = response.data;
	}

	$('#plan-add-button').on("click", function () {
		$("#plans-table").hide();
		let exercises = {};
		let users = {};

		html = ' \
		<form id="new-plan-form"> \
			<div class="form-group"> \
				<label for="plan-name">Plan Name</label> \
				<input type="text" class="form-control" name="plan-name" placeholder="Enter plan name" required> \
			</div> \
			<div class="form-group"> \
				<label for="plan-description">Description</label> \
				<textarea type="text" class="form-control" cols="40" rows="5" name="plan-description" id="description" placeholder="Enter description" required></textarea> \
			</div> \
			<div class="form-group"> \
				<label for="difficulty">Select Difficulty</label> \
				<select class="form-control" name="plan-difficulty" id="difficulty"> \
					<option>1</option> \
					<option>2</option> \
					<option>3</option> \
					<option>4</option> \
					<option>5</option> \
				</select> \
			</div> \
			<button class="btn btn-success" id="add-days-button">Add days</button> \
		</form>';

		$("#plan-canvas").html(html);
	});

	$('#workout-add-button').on("click", function () {
		$.ajax({
			type: "GET",
			dataType: "json",
			url: 'http://localhost:9000/plans',
			complete: function (xhr) {
				var plans = $.parseJSON(xhr.responseText).data;

				html = ' \
				<div class="modal fade" id="addnewworkoutmodal" tabindex="-1" role="dialog"  aria-labelledby="exampleModalCenterTitle" aria-hidden="true"> \
				<div class="modal-dialog modal-dialog-centered" role="document"> \
					<div class="modal-content"> \
						<div class="modal-header"> \
							<h5 class="modal-title" id="exampleModalCenterTitle">Add new workout</h5> \
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"> \
								<span aria-hidden="true">&times;</span> \
							</button> \
						</div> \
						<div class="modal-body">\
						<form id="new-workout-form"> \
							<div class="form-group"> \
								<label for="plan-name">Select Plan</label> \
								<select class="form-control" id="workout-plan-id">';

				$.each(plans, function (key, value) {
					html += "<option value=" + value.id + ">" + value.plan_name + "</option>";
				});

				html += '</select></div>';

				$.ajax({
					type: "GET",
					dataType: "json",
					url: 'http://localhost:9000/users',
					complete: function (xhr) {
						var users = $.parseJSON(xhr.responseText).data;
						console.log(users);
						html += '<div class="form-group"> \
								<label for="plan-name">Select User</label> \
								<select class="form-control" id="workout-user-id">';

						$.each(users, function (key, value) {
							html += "<option value=" + value.id + ">" + value.first_name + ' - ' + value.email + "</option>";
						});

						html += '</select></div> \
								<button type="submit" class="btn btn-primary" id="new-workout-button" data-dismiss="modal">Submit</button> \
								<button type="button" class="btn btn-warning" data-dismiss="modal">Close</button> \
								</form>\
								</div>\
							</div></div>';

						$("#new-workout-stuff").html(html);
						$('#addnewworkoutmodal').modal('show');
					}
				});
			}
		});
	});

	$("#new-workout-stuff").on('click', "#new-workout-button", function (e) {
		console.log("clicked it");
		e.preventDefault();

		let formData = {
			'plan_id': $('#workout-plan-id').val(),
			'user_id': $('#workout-user-id').val(),
		};

		console.log(formData);

		$.ajax({
			type: 'post',
			url: 'http://localhost:9000/workouts',
			data: formData,
			success: function () {
				showAllWorkouts();
			}
		});
	});

	$("#plan-canvas").on('submit', "#new-plan-form", function (e) {
		e.preventDefault();
	});

	$('#plan-canvas').on('click', '#add-days-button', function (e) {
		e.preventDefault();

		let id = $(this).closest('tr').attr('id');
		let formData = {
			'name': $('input[name=plan-name]').val(),
			'description': $('#description').val(),
			'difficulty': $('#difficulty').val() //$(this).find("option:selected").text();
		};

		if ($('input[name=plan-name]').val() == "" || $('#description').val() == "" || $('#difficulty').val() == "") {
			alert('Please fill out the form first');
			return;
		}

		console.log(formData);

		$.ajax({
			type: "POST",
			dataType: "json",
			url: 'http://localhost:9000/plans',
			data: formData,
			complete: function (xhr) {
				var new_plan_id = $.parseJSON(xhr.responseText).data.id;
				console.log(new_plan_id);

				location.href = "plans?new_id=" + new_plan_id;
			}
		});
	});

	function editPlan(plan_id) {
		html = ' <h4><strong>Add a new day to the plan<strong></h4>\
					<form id="new-day-form">' +
			'<input type="hidden" class="form-control" name="plan-id" id="plan-id" value="' + plan_id + '">' +
			'<div class="form-group"> \
							<label for="day-name">Day Name</label> \
							<input type="text" class="form-control" name="day-name" placeholder="Enter day name" required> \
						</div>';

		$('#plan-canvas').html(html);

		$.ajax({
			type: "GET",
			dataType: "json",
			url: 'http://localhost:9000/exercises',
			complete: function (xhr, exercises) {
				exercises = $.parseJSON(xhr.responseText).data;
				console.log(exercises);

				html = '<div class="form-group"> \
									<label for="exercises">Select exercises</label> \
									<select multiple class="form-control" name="day-exercises" id="day-exercises">';

				$.each(exercises, function (key, value) {
					html += '<option value="' + value.id + '">' + value.name + '</option>';
				});
				html += '</select> \
								</div> \
								<button class="btn btn-success" id="add-new-days-button-done">Done</button> \
								<button class="btn btn-primary" id="add-new-days-button">Add more days</button> \
							</form>';

				$('#plan-canvas').append(html);
			}
		});
	}

	function addNewDay() {
		let id = $('input[name=plan-id]').val(),
			exercises = $("#day-exercises").val(),
			day_name = $('input[name=day-name]').val();

		$.ajax({
			type: "PUT",
			dataType: "json",
			data: {
				'plan_id': id,
				'day_name': day_name,
				'exercises': exercises
			},
			url: 'http://localhost:9000/plans/day',
			success: function (response) {
				$("#plans").click();
				$("#plan-canvas").hide();
			}
		});
	}

	$('#plan-canvas').on('click', '#add-new-days-button-done', function () {
		addNewDay();
		showAllPlans();
	});

	$('#plan-canvas').on('click', '#add-new-days-button', function () {
		addNewDay();
		let id = $('input[name=plan-id]').val();
		location.href = "plans?new_id=" + id;
	});

	$('#plans-table').on('click', '#plan-delete-button', function () {
		let id = $(this).closest('tr').attr('id');

		$.ajax({
			type: "DELETE",
			dataType: "json",
			url: 'http://localhost:9000/plans?plan_id=' + id,
			success: function (response) {
				console.log('done, will now click');
				$("#plans").click();
			}
		});
	});

	$('#users-table').on('click', '#user-delete-button', function () {
		let id = $(this).closest('tr').attr('id');

		$.ajax({
			type: "DELETE",
			dataType: "json",
			url: 'http://localhost:9000/users?user_id=' + id,
			success: function (response) {
				$("#users").click();
			}
		});
	});

	function showAllUsers() {
		let target = $(this).data("target");

		$.ajax({
			type: "GET",
			dataType: "json",
			url: `http://localhost:9000/users`,
			success: function (response) {
				console.log(response);
				let trHTML = '<tr><th align="left" width="50">#</th><th align="left" width="100">First Name</th><th align="left" width="100">Last Name</th><th align="left" width="100">Email</th><th></th><th></th></tr>';

				let data = response.data;
				$.each(data, function (key, value) {
					trHTML +=
						'<tr id="' + value.id + '"><td>' + value.id +
						'</td><td>' + value.first_name +
						'</td><td>' + value.last_name +
						'</td><td>' + value.email +
						'</td><td><a class="btn btn-danger" id="user-delete-button" href="#">Delete</a>' +
						'</td></tr>';
				});

				$('#users-table').html(trHTML);
			}
		});
	}

	function showAllExercises() {
		let target = $(this).data("target");

		$.ajax({
			type: "GET",
			dataType: "json",
			url: `http://localhost:9000/exercises`,
			success: function (response) {
				console.log(response);
				let trHTML = '<tr><th align="left" width="50">#</th><th align="left" width="100">Exercise name</th></th><th></th><th></th></tr>';

				let data = response.data;
				let i = 1;
				$.each(data, function (key, value) {
					trHTML +=
						'<tr id="' + value.id + '"><td>' + i +
						'</td><td>' + value.name +
						'</td><td><a class="btn btn-danger" id="exercise-delete-button" href="">Delete</a>' +
						'</td></tr>';
					i++;
				});

				$('#exercises-table').html(trHTML);
			}
		});
	}

	$('#new-exercise-button').on('click', function (e) {

		e.preventDefault();

		let formData = {
			'exercise_name': $('input[name=exercise-name]').val()
		};

		console.log(formData);
		if (formData.name == '') {
			return;
		}

		$.ajax({
			type: 'post',
			url: 'http://localhost:9000/exercises',
			data: formData,
			success: function () {
				showAllExercises();
			}
		});
	});

	$('#exercises-table').on('click', '#exercise-delete-button', function () {
		let id = $(this).closest('tr').attr('id');

		$.ajax({
			type: "DELETE",
			dataType: "json",
			url: 'http://localhost:9000/exercises?exercise_id=' + id,
			success: function (response) {
				showAllExercises();
			}
		});
	});

	$('#users-table').on('click', '#user-edit-button', function () {
		let id = $(this).closest('tr').attr('id');

		$.ajax({
			type: "DELETE",
			dataType: "json",
			url: 'http://localhost:9000/exercises?exercise_id=' + id,
			success: function (response) {

			}
		});

		$("#users").click();
	});

	$('#users-table').on('click', '#user-delete-button', function () {
		let id = $(this).closest('tr').attr('id');

		$.ajax({
			type: "DELETE",
			dataType: "json",
			url: 'http://localhost:9000/users?user_id=' + id,
			success: function (response) {

			}
		});

		$("#users").click();
	});

	$('#users-table').on('click', '#user-delete-button', function () {
		let id = $(this).closest('tr').attr('id');

		$.ajax({
			type: "DELETE",
			dataType: "json",
			url: 'http://localhost:9000/users?id=' + id,
			success: function (response) {

			}
		});
	});

	function showPlan(plan_id) {
		$.ajax({
			type: "GET",
			dataType: "json",
			url: 'http://localhost:9000/plans?id=' + plan_id,
			success: function (response) {
				console.log(response);
				let html = '';
				let data = response.data;

				html = '<div class="form-group">' +
					'<h2><b>' + data.name + '</b> - ' + data.difficulty + '/5</h2><br>' +
					'<p>' + data.description + '</p><br>';
				let i = 1;
				$.each(data.days, function (key, value) {
					html += '<div class="card" style="width: 18rem;"><div class="card-header list-group-item-info"><strong> Day ' + i + ' - ' + value.name + '</strong></div><ul class="list-group list-group-flush">';
					$.each(value.exercises, function (key, value) {
						html += '<li class="list-group-item">' + value.name + '</li>';
					});
					html +="</ul></div><br><br>"
					i++;
				});

				html += '</div>';

				$("#plan-add-button").hide();
				$("#view-plan").html(html);
				$("#view-plan").show();
				$("#plans-table").hide();
				console.log("yyyy");
			}
		});
	}
});